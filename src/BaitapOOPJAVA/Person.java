package BaitapOOPJAVA;

import java.util.Scanner;

public class Person {
	//khai bao bien
	public String name;
	public String address;
	public int age;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	//Nhap thong tin tu ban phim
	public void inputDetails() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Vui long nhap ten :");
		this.name = sc.nextLine();
		System.out.println("Vui long nhap dia chi :");
		this.address = sc.nextLine();
		System.out.println("Vui long nhap tuoi :");
		this.age = sc.nextInt();
	}
	//display information employee( hien thi thong tin)
	public void displaytDetails() {
		System.out.println("Name: " + this.getName());
		System.out.println("Address : " + this.getAddress());
		System.out.println("Age : " + this.getAge());
		
	}

}
