package BaitapOOPJAVA;

public class Car {
	public double price;
	public String color;
	public String type;
	public Car() {
		super();
	}
	public Car(double price, String color, String type) {
		super();
		this.price = price;
		this.color = color;
		this.type = type;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	
	@Override
	public String toString() {
		return "Car [price=" + price + ", color=" + color + ", type=" + type + "]";
	}
	public void displaytDetails() {
		//System.out.println("price:"+ price + "color"+ color + "type" +type);
	
		System.out.println(this.toString());
	}
	public void displaytDetails2(int price) {
		//System.out.println("price:"+ price + "color"+ color + "type" +type);
	
		System.out.println(price);
	}
	
	

}
