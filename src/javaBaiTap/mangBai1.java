package javaBaiTap;

import java.util.Scanner;

public class mangBai1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Bài 1: Viết một chương trình Java cho phép nhập vào 1 mảng các số thực, đồng
		 * thời in ra phần tử có giá trị lớn nhất và nhỏ nhất của mảng này. Hãy tính và
		 * in ra trung bình cộng của các phần tử của mảng có giá trị chia hết cho 5. Hãy
		 * đếm số phần tử có giá trị bằng với phần tử lớn nhất trong mảng. Hãy tính và
		 * in ra tích của các phần tử trong mảng. Hãy đếm xem mảng có bao nhiêu phần tử
		 * chia hết cho 3.
		 */
		int n = 0;

		Scanner sc = new Scanner(System.in);
		System.out.println("So phan tu trong mang n = ");
		n = sc.nextInt();
		int arr[] = new int[n];
		System.out.println("Nhap phan tu trong mang");
		for (int i = 0; i < n; i++) {
			System.out.print("arr[" + i + "]=");
			arr[i] = sc.nextInt();
		}
		System.out.println("mang phan tu la:");
		for (int i = 0; i < n; i++) {
			System.out.print(arr[i] + "\n");
		}
		double max = arr[0], min = arr[0],tich = 0;
		float trungBinh = 0f, tong = 0;
		int dem5 = 0,demMax = 0,dem3=0;
		for (int i = 0; i < n; i++) {
			tich = tich*arr[i];
			if (arr[i] > max) {
			max = arr[i];
				
			}
			if (arr[i] < min) {
				min = arr[i];
			}
			if (arr[i] % 5 == 0) {
				dem5 = dem5 + 1;
				tong += arr[i];
				trungBinh = tong/dem5;
			}
			if (arr[i] % 3 == 0) {
				dem3 = dem3 + 1;
			}
		
		}
		System.out.println("Gia tri lon nhat trong mang la :" + max);
		System.out.println("Gia tri nho nhat trong mang la :" + min);
		System.out.println("trung binh cong cac so chia het cho 5 :" + trungBinh);
		System.out.println("Tich cac phan tu trong mang =" + tich);
		System.out.println("so phan tu chia het cho 3 la:"+ dem3);
	}

}
