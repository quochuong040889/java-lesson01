package javaBaiTap;

import java.util.Scanner;

public class mangBaiTap2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Bài 2: Viết một chương trình Java cho phép làm việc với mảng hai chiều. Khai
		 * báo một mảng bao gồm 3 dòng, 4 cột, các phần tử của mảng là số nguyên. Hãy
		 * thực hiện các công việc sau: Nhập giá trị cho các phần tử của mảng In ra giá
		 * trị của các phần tử mảng Tính và in ra tổng của các phần tử trong mảng
		 */
		int[][] arr = new int[3][4];
		System.out.println("Nhap cac phan tu trong mang:");
		Scanner sc = new Scanner(System.in);
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print("arr[" + i + "][" + j + "]=");
				arr[i][j] = sc.nextInt();

			}
		}
		System.out.println("mang la : \n");
		int tong = 0;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 4; j++) {
				System.out.print(arr[i][j] + "\n ");
				tong +=arr[i][j];
			}
			
		}
		System.out.println("Tong ca phan tu trong mang ="+ tong);
		

	}
}
