package javaBaiTap;

import java.util.Scanner;

public class mangBai3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 * Bài 3: Viết một Chương trình cho phép nhập vào một chuỗi và thao tác với lớp
		 * String. Sau đó in ra: Độ dài của chuỗi. Lấy ra ký tự ở vị trí thứ 3 trong
		 * chuỗi. Lấy ra chỉ số của ký tự ‘a’ trong chuỗi. Thay thế các ký tự ‘a’ trong
		 * chuỗi bằng ký tự ‘b’. Đếm tổng số ký tự ‘c’ trong chuỗi. Chuyển chuỗi thành
		 * lowerCase/uppercase. Cắt khoảng trắng hai bên chuỗi. In ra một chuỗi con
		 * trong chuỗi, bắt đầu từ ký tự thứ 5. Đếm tổng số nguyên âm và phụ âm trong
		 * chuỗi. Nối chuỗi ‘Programming’ vào cuối của chuỗi hiện tại. Lấy ra chỉ số
		 * cuối cùng của chuỗi “abc” trong chuỗi hiện tại. Nhập vào một chuỗi khác, rồi
		 * so sánh chuỗi hiện tại với chuỗi được nhập.
		 * 
		 * 
		 */
		String chuoi = "";
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhap chuoi :");
		chuoi = sc.nextLine();
		char kitu[] = chuoi.toCharArray();
		System.out.println("Do dai chuoi = " + chuoi.length());
		// Lấy ra ký tự ở vị trí thứ 3 trong chuỗi
		char kitu3 = chuoi.charAt(2);
		System.out.println("ki tu vi tri so 3 trong chuoi la :" + kitu3);
		// Lấy ra chỉ số của ký tự ‘a’ trong chuỗi
		for (int i = 0; i < chuoi.length(); i++) {
			if (chuoi.charAt(i) == 'a') {
				System.out.println("Vi tri cua ki tu a trong chuoi la :" + i);
			}
		}
		// Thay thế các ký tự ‘a’ trong chuỗi bằng ký tự ‘b’.
		String kitub = "";
		kitub = chuoi.replace('a', 'b');
		System.out.print("choi moi la : " + kitub);
		// Đếm tổng số ký tự ‘c’ trong chuỗi
		int dem = 0;
		for (int i = 0; i < chuoi.length(); i++) {
			if (chuoi.charAt(i) == 'c') {
				dem = dem + 1;
			}

		}
		System.out.println("tong ki tu c trong chuoi la :" + dem);
		// Chuyển chuỗi thành
		// * lowerCase/uppercase
		String chuoi4;
		chuoi4 = chuoi.toLowerCase();
		System.out.println("Ham toLowerCase : " + chuoi4);

		String chuoi5;
		chuoi5 = chuoi.toUpperCase();
		System.out.println("Ham toUpperCase : " + chuoi5);
		// Cắt khoảng trắng hai bên chuỗi
		String chuoi6;
		chuoi6 = chuoi.trim();
		System.out.println("Chuoi sau khi bo khoang trang 2 ben:" + chuoi6);
		// In ra một chuỗi con
		// * trong chuỗi, bắt đầu từ ký tự thứ 5
		String chuoi7 = "";
		for (int i = 5; i < chuoi.length(); i++) {
			chuoi7 += chuoi.charAt(i);
		}
		System.out.println("Chuoi in tu phan tu 5 la :" + chuoi7);
		// Nối chuỗi ‘Programming’ vào cuối của chuỗi hiện tại
		String chuoi8 = "Programming";
		String newChuoi;
		newChuoi = chuoi8.concat(chuoi);
		System.out.println("Chuoi sau khi noi la:" + newChuoi);
		System.out.println("chuoi moi sau khi noi la: " + chuoi + " Programming");
		// Lấy ra chỉ số
		// cuối cùng của chuỗi “abc” trong chuỗi hiện tại
		int v,f;
		v = chuoi.lastIndexOf("abc");// nguoc lai la IndexOf()
		f = chuoi.indexOf("ie");
		System.out.print("\n vi tri cuoi cung cua chuoi abc la: " + v);
		System.out.print("\n vi tri dau tien cua chuoi ie la: " + f);
		//so sánh chuỗi
		String chuoimoi;
		System.out.print("\n  chuoi moi:" );
		chuoimoi = sc.nextLine(); 
		if(chuoi.contentEquals(chuoimoi)== true)
		{
			System.out.print("\n hai chuoi giong nhau ");
		}
		else
		{
			System.out.print("\n hai chuoi khac nhau ");
		}
		

	}

}
